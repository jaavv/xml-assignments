<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<!--define template targetting root element-->
	<!-- Well done here, Marc. Your xsl properly formats and outputs the xml data.
You've got the obligatory comment I needed to see.  Great work
10/10
-->
	<xsl:template match="/">
		<!--begin html code-->
		<html>
			<head>
				<title>XML Assignment 3</title>
			</head>
			<body>
				<h1>Customer Information</h1>
				<!--table for customer information-->
				<table border="1">
					<tbody>
						<tr>
							<td>
								<strong>Name</strong>
							</td>
							<td>
								<xsl:value-of select="telephoneBill/customer/name"/>
							</td>
						</tr>
						<tr>
							<td>
								<strong>Address</strong>
							</td>
							<td>
								<xsl:value-of select="telephoneBill/customer/address"/>
							</td>
						</tr>
						<tr>
							<td>
								<strong>City</strong>
							</td>
							<td>
								<xsl:value-of select="telephoneBill/customer/name"/>
							</td>
						</tr>
						<tr>
							<td>
								<strong>Province</strong>
							</td>
							<td>
								<xsl:value-of select="telephoneBill/customer/province"/>
							</td>
						</tr>
					</tbody>
				</table>
				<p/>
				<!--table for call information-->
				<table border="1">
					<tbody>
						<tr>
							<th>Called Number</th>
							<th>Date</th>
							<th>Duration In Minutes</th>
							<th>Charge</th>
						</tr>
						<xsl:for-each select="telephoneBill/call">
							<tr>
								<!--color every second row-->
								<xsl:if test="position() mod 2=0">
									<xsl:attribute name="bgcolor">#EAEAEA</xsl:attribute>
								</xsl:if>
								<!--extract value of XML Element and output to table cell-->
								<td>
									<xsl:value-of select="@number"/>
								</td>
								<td>
									<xsl:value-of select="@date"/>
								</td>
								<td>
									<xsl:value-of select="@durationInMinutes"/>
								</td>
								<td>
									<xsl:value-of select="@charge"/>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
